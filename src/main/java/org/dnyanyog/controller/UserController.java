package org.dnyanyog.controller;

import java.util.List;

import javax.validation.Valid;

import org.dnyanyog.dto.request.UserRequest;
import org.dnyanyog.dto.response.UserResponse;
import org.dnyanyog.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class UserController {

	@Autowired
	UserService userService;

	@PostMapping(path = "api/v1/addUser")
	public ResponseEntity<UserResponse> addUser(@RequestBody @Valid UserRequest request, BindingResult bindingResult) {
		if (bindingResult.hasErrors()) {
			UserResponse errorResponse = new UserResponse();
			errorResponse.setResponseCode(HttpStatus.UNPROCESSABLE_ENTITY.value());
			errorResponse.setResponseMessage("Validation failed");
            return ResponseEntity.status(HttpStatus.UNPROCESSABLE_ENTITY).body(errorResponse);
		}

		ResponseEntity<UserResponse> response = userService.addUser(request);
		return response;
	}

	@GetMapping(path = "api/v1/user/{userName}")
	public ResponseEntity<UserResponse> getUserByUsername(@PathVariable String userName) {
		return userService.getUserResponseByUsername(userName);
	}

	@GetMapping(path = "api/v1/users")
	public ResponseEntity<List<UserResponse>> getAllUsers() {
		return userService.getAllUsers();
	}

	@DeleteMapping(path = "api/v1/deleteUser/{userName}")
	public ResponseEntity<UserResponse> deleteUserByUsername(@PathVariable String userName) {
		return userService.deleteUserByUsername(userName);
	}

	@PostMapping(path = "api/v1/updateUser")
	public ResponseEntity<UserResponse> updateUser(@RequestBody UserRequest request) {
		return userService.updateUser(request);
	}
	
	@DeleteMapping(path = "api/v2/deleteUser/{userName}")
    public ResponseEntity<UserResponse> deleteUserAndSetInactive(@PathVariable String userName) {
        return userService.deleteUserAndSetInactive(userName);
    }

}
