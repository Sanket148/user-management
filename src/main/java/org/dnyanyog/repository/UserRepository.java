package org.dnyanyog.repository;

import java.util.Optional;

import org.dnyanyog.entity.Users;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRepository extends JpaRepository<Users, Long> {

	 Optional<Users> findByuserId (long userId );
	
	Users findByuserName(String  userName);

	Users findByPassword(String password);

	Users findByuserCategory(String userCategory);
}
