package org.dnyanyog.dto.response;

import java.util.List;

import org.springframework.stereotype.Component;

import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@Component
@JsonPropertyOrder({"responseCode", "responseMessage", "data", "error"})
public class UserResponse {
	private int ResponseCode;
	private String ResponseMessage;
	private UserData data;
	private List<UserError> error;

	

	

	public int getResponseCode() {
		return ResponseCode;
	}

	public void setResponseCode(int responseCode) {
		ResponseCode = responseCode;
	}

	public String getResponseMessage() {
		return ResponseMessage;
	}

	public void setResponseMessage(String responseMessage) {
		ResponseMessage = responseMessage;
	}

	public List<UserError> getError() {
		return error;
	}

	public void setError(List<UserError> error) {
		this.error = error;
	}

	public UserData getData() {
		return data;
	}

	public void setData(UserData userData) {
		this.data = userData;

	}

	public static class UserError {
		private String message;
		private String field;


		public String getField() {
			return field;
		}

		public void setField(String field) {
			this.field = field;
		}

		public String getMessage() {
			return message;
		}

		public void setMessage(String message) {
			this.message = message;
		}

	public UserError(String field, String message) {
		this.field = field;
		this.message = message;
	}
	}

}
