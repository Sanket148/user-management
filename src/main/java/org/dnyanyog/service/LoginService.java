package org.dnyanyog.service;

import org.dnyanyog.dto.request.LoginRequest;
import org.dnyanyog.dto.response.LoginResponse;
import org.dnyanyog.entity.Users;
import org.dnyanyog.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

@Service
public class LoginService {

	@Autowired
	private UserRepository userRepository;

	public ResponseEntity<LoginResponse> loginUser(LoginRequest request) {
		Users user = userRepository.findByuserName(request.getUserName());

		if (user == null) {
			return createErrorResponse(404, "Account not found");
		}

		if (!user.getPassword().equals(request.getPassword())) {
			return createErrorResponse(401, "Invalid username or password.");
		}

		if (!user.getUserStatus().equals("ACTIVE")) {
			if (user.getUserStatus().equals("INACTIVE")) {
				return createErrorResponse(401, "Account inactive.");
			}
			if (user.getUserStatus().equals("LOCKED")) {
				return createErrorResponse(405, "Account is locked");
			}
			if (user.getUserStatus().equals("EXPIRED")) {
				return createErrorResponse(405, "Account is locked");
			}
		}

		return ResponseEntity.ok(createSuccessResponse());
	}

	private ResponseEntity<LoginResponse> createErrorResponse(int code, String message) {
		LoginResponse response = new LoginResponse();
		response.setResponseCode(code);
		response.setResponseMessage(message);
		return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body(response);
	}

	private LoginResponse createSuccessResponse() {
		LoginResponse response = new LoginResponse();
		response.setResponseCode(HttpStatus.CREATED.value());
		response.setResponseMessage("Login successful");
		return response;
	}

}