package org.dnyanyog.service;

import java.util.ArrayList;
import java.util.List;
import org.dnyanyog.dto.request.UserRequest;
import org.dnyanyog.dto.response.UserData;
import org.dnyanyog.dto.response.UserResponse;
import org.dnyanyog.entity.Users;
import org.dnyanyog.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

@Service
public class UserService {

	@Autowired
	UserResponse userResponse;

	@Autowired
	UserRepository userRepository;

	@Autowired
	Users user;

//add User
	public ResponseEntity<UserResponse> addUser(UserRequest request) {
		userResponse = new UserResponse();
		userResponse.setData(new UserData());

		List<UserResponse.UserError> validationErrors = new ArrayList();

		if (StringUtils.isEmpty(request.getUserName())) {
			validationErrors.add(new UserResponse.UserError("UserName", "UserName is required"));
		}
		if (StringUtils.isEmpty(request.getUserName())) {
			validationErrors.add(new UserResponse.UserError("UserName", "UserName is required"));
		}

		if (StringUtils.isEmpty(request.getPassword())) {
			validationErrors.add(new UserResponse.UserError("Password", "Password is required"));
		}
		if (StringUtils.isEmpty(request.getUserCategory())) {
			validationErrors.add(new UserResponse.UserError("UserCategory", "UserCategory is required"));
		}
		if (StringUtils.isEmpty(request.getUserStatus())) {
			validationErrors.add(new UserResponse.UserError("UserStatus", "UserStatus is required"));
		}

		if (!validationErrors.isEmpty()) {
			UserResponse response = new UserResponse();
			response.setResponseCode(HttpStatus.UNPROCESSABLE_ENTITY.value());
			response.setResponseMessage("Validation failed");
			response.setError(validationErrors);
			return ResponseEntity.status(HttpStatus.UNPROCESSABLE_ENTITY).body(response);
		}
		if (userRepository.findByuserName(request.getUserName()) != null) {
			UserResponse response = new UserResponse();
			response.setResponseCode(HttpStatus.CONFLICT.value());
			response.setResponseMessage("UserName already exists");
			return ResponseEntity.status(HttpStatus.CONFLICT).body(response);
		}

		user = new Users();
		user.setUserId(request.getUserId());
		user.setUserName(request.getUserName());
		user.setPassword(request.getPassword());
		user.setUserCategory(request.getUserCategory());
		user.setUserStatus(request.getUserStatus());

		user = userRepository.saveAndFlush(user);

		userResponse.setResponseCode(HttpStatus.CREATED.value());
		userResponse.setResponseMessage("Success");
		userResponse.getData().setUserId(user.getUserId());
		userResponse.getData().setUserName(user.getUserName());
		userResponse.getData().setPassword(user.getPassword());
		userResponse.getData().setUserCategory(user.getUserCategory());
		userResponse.getData().setUserStatus(user.getUserStatus());

		return ResponseEntity.status(HttpStatus.CREATED).body(userResponse);
	}

	// search user by username
	public ResponseEntity<UserResponse> getUserResponseByUsername(String userName) {
		Users user = userRepository.findByuserName(userName);

		if (user == null) {
			UserResponse response = new UserResponse();
			response.setResponseCode(HttpStatus.NOT_FOUND.value());
			response.setResponseMessage("User not found");
			return ResponseEntity.status(HttpStatus.NOT_FOUND).body(response);
		}

		UserResponse response = new UserResponse();
		response.setResponseCode(HttpStatus.OK.value());
		response.setResponseMessage("User found");
		response.setData(new UserData());
		response.getData().setUserId(user.getUserId());
		response.getData().setUserName(user.getUserName());
		response.getData().setPassword(user.getPassword());
		response.getData().setUserCategory(user.getUserCategory());
		response.getData().setUserStatus(user.getUserStatus());

		return ResponseEntity.status(HttpStatus.OK).body(response);
	}

	// search all users
	public ResponseEntity<List<UserResponse>> getAllUsers() {
		List<Users> allUsers = userRepository.findAll();

		List<UserResponse> userResponses = new ArrayList<>();
		for (Users user : allUsers) {
			UserResponse response = new UserResponse();
			response.setResponseCode(HttpStatus.OK.value());
			response.setResponseMessage("User found");
			response.setData(new UserData());
			response.getData().setUserId(user.getUserId());
			response.getData().setUserName(user.getUserName());
			response.getData().setPassword(user.getPassword());
			response.getData().setUserCategory(user.getUserCategory());
			response.getData().setUserStatus(user.getUserStatus());

			userResponses.add(response);
		}

		return ResponseEntity.status(HttpStatus.OK).body(userResponses);
	}

	// delete user
	public ResponseEntity<UserResponse> deleteUserByUsername(String userName) {
		Users user = userRepository.findByuserName(userName);

		if (user == null) {
			UserResponse response = new UserResponse();
			response.setResponseCode(HttpStatus.NOT_FOUND.value());
			response.setResponseMessage("User not found");
			return ResponseEntity.status(HttpStatus.NOT_FOUND).body(response);
		}

		userRepository.delete(user);

		UserResponse response = new UserResponse();
		response.setResponseCode(HttpStatus.OK.value());
		response.setResponseMessage("User record deleted successfully");
		return ResponseEntity.status(HttpStatus.OK).body(response);
	}

	// update user
	public ResponseEntity<UserResponse> updateUser(UserRequest request) {
		Users user = userRepository.findByuserName(request.getUserName());

		if (user == null) {
			UserResponse response = new UserResponse();
			response.setResponseCode(HttpStatus.NOT_FOUND.value());
			response.setResponseMessage("User not found");
			return ResponseEntity.status(HttpStatus.NOT_FOUND).body(response);
		}

		// Update the user
		user.setPassword(request.getPassword());
		user.setUserCategory(request.getUserCategory());
		user.setUserStatus(request.getUserStatus());
		userRepository.saveAndFlush(user);

		UserResponse response = new UserResponse();
		response.setResponseCode(HttpStatus.OK.value());
		response.setResponseMessage("User record updated successfully");
		response.setData(new UserData());
		response.getData().setUserId(user.getUserId());
		response.getData().setUserName(user.getUserName());
		response.getData().setPassword(user.getPassword());
		response.getData().setUserCategory(user.getUserCategory());
		response.getData().setUserStatus(user.getUserStatus());

		return ResponseEntity.status(HttpStatus.OK).body(response);
	}

	// Delete user and set userStatus to "INACTIVE"
    public ResponseEntity<UserResponse> deleteUserAndSetInactive(String userName) {
        Users user = userRepository.findByuserName(userName);

        if (user == null) {
            UserResponse response = new UserResponse();
            response.setResponseCode(HttpStatus.NOT_FOUND.value());
            response.setResponseMessage("User not found");
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(response);
        }

        // Update the user status to "INACTIVE"
        user.setUserStatus("INACTIVE");
        userRepository.saveAndFlush(user);

        UserResponse response = new UserResponse();
        response.setResponseCode(HttpStatus.OK.value());
        response.setResponseMessage("User status set to INACTIVE");
//        response.setData(new UserData());
//        response.getData().setUserId(user.getUserId());
//        response.getData().setUserName(user.getUserName());
//        response.getData().setPassword(user.getPassword());
//        response.getData().setUserCategory(user.getUserCategory());
//        response.getData().setUserStatus(user.getUserStatus());

        return ResponseEntity.status(HttpStatus.OK).body(response);
    }


}
